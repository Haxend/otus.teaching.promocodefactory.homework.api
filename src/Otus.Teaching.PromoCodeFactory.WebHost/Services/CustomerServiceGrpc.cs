﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Grpc.Core;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.WebHost.Mappers;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Services
{
    public class CustomerServiceGrpc : Customer.CustomerBase
    {
        private readonly IRepository<Core.Domain.PromoCodeManagement.Customer> _customerRep;
        private readonly IRepository<Preference> _preferenceRep;

        public CustomerServiceGrpc(IRepository<Core.Domain.PromoCodeManagement.Customer> customerRep
            , IRepository<Preference> preferenceRep)
        {
            _customerRep = customerRep;
            _preferenceRep = preferenceRep;
        }

        public override async Task<CustomerShortResponseListGrpc> GetCustomers(NoParameter request, ServerCallContext context)
        {
            var customerShortResponseListGrpc = new CustomerShortResponseListGrpc();
            var customers = await _customerRep.GetAllAsync();
            
            var result = customers.Select(p => new CustomerShortResponseGrpc()
            {
                Id = p.Id.ToString(),
                Email = p.Email,
                FirstName = p.FirstName,
                LastName = p.LastName,
            });
            customerShortResponseListGrpc.CustomersShortResponse.AddRange(result);
            
            return customerShortResponseListGrpc;
        }

        public override async Task<CustomerResponseGrpc> GetCustomer(CustomerId request, ServerCallContext context)
        {
            var customer = await _customerRep.GetByIdAsync(Guid.Parse(request.Id));

            if (customer == null)
                throw new Exception("Customer not found");
            
            var customerGrpc = new CustomerResponseGrpc
            {
                Id = customer.Id.ToString(),
                Email = customer.Email,
                FirstName = customer.FirstName,
                LastName = customer.LastName
            };

            if (customer.Preferences.Any())
            {
                customerGrpc.Preferences.AddRange(customer.Preferences.Select(x => new PreferenceResponseGrpc
                {
                    Id = x.PreferenceId.ToString(),
                    Name = x.Preference.Name
                }));
            }

            return customerGrpc;
        }

        public override async Task<CustomerResponseGrpc> CreateCustomer(CreateOrEditCustomerRequestGrpc request, ServerCallContext context)
        {
            var preferences = await _preferenceRep.GetRangeByIdsAsync(request.PreferenceIds.Select(x => Guid.Parse(x.Id)).ToList());

            Core.Domain.PromoCodeManagement.Customer customer = CustomerMapper.MapFromModel(request, preferences);

            await _customerRep.AddAsync(customer);

            var res = new CustomerResponseGrpc
            {
                Id = customer.Id.ToString()
            };

            return res;

        }

        public override async Task<CustomerResponseGrpc> EditCustomer(CreateOrEditCustomerRequestGrpc request, ServerCallContext context)
        {
            var customer = await _customerRep.GetByIdAsync(Guid.Parse(request.Id));

            if (customer == null)
                throw new Exception("Customer not found");

            var preferences = await _preferenceRep.GetRangeByIdsAsync(request.PreferenceIds.Select(x => Guid.Parse(x.Id)).ToList());

            customer = CustomerMapper.MapFromModel(request, preferences, customer);

            await _customerRep.UpdateAsync(customer);

            var customerGrpc = await GetCustomer(new CustomerId { Id = request.Id }, context);

            return customerGrpc;
        }

        public override async Task<NoParameter> DeleteCustomer(CustomerId request, ServerCallContext context)
        {
            var customer = await _customerRep.GetByIdAsync(Guid.Parse(request.Id));

            if (customer == null)
                throw new Exception("Customer not found");

            await _customerRep.DeleteAsync(customer);

            return new NoParameter();
        }
    }
}
